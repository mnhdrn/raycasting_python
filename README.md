# Raycasting_python

A simple C like raycaster in Python with SDL2 library.

It could be more *python* but you know,  I came from C programming and this was my first project in python !


#### Installation

*Please install Python3* then the SDL2 python library ```sudo pip3 install PySDL2```

then simply launch:  ```./raycast.py map```

use 'WASD' to move on the map !
