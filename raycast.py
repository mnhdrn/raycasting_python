#!/usr/bin/python3
import sys
import time
import math
import ctypes
import sdl2.ext
from sdl2 import *

#==============================================================================#
#------------------------------------------------------------------------------#
# RAYCASTING FUNCTION


def hit_hor(ray_angle):
    if ray_angle == ANGLE_0 or ray_angle == ANGLE_180:
        return 4294967295

    if ray_angle > 0 and ray_angle < ANGLE_180:
        hor_y = (p_map[0] * GRID_SIZE) + GRID_SIZE
        hor_x = p_pos[1] + TANTABLE_I[ray_angle] * (hor_y - p_pos[0])
        next_y = GRID_SIZE
        next_x = STEPTABLE_X[ray_angle]
    else:
        hor_y = (p_map[0] * GRID_SIZE)
        hor_x = p_pos[1] + TANTABLE_I[ray_angle] * (hor_y - p_pos[0])
        hor_y -= 1
        next_y = -GRID_SIZE
        next_x = STEPTABLE_X[ray_angle]

    while True:
        gy = int(hor_y / GRID_SIZE)
        gx = int(hor_x / GRID_SIZE)
        if (gy < 0 or gy > GRID_H) or (gx < 0 or gx > GRID_W):
            return 2147483647
        elif MAP[gy][gx] == '1':
            ret = (hor_x - p_pos[1]) / COSTABLE[ray_angle]
            return ret
        hor_y += next_y
        hor_x += next_x


def hit_ver(ray_angle):
    if ray_angle == ANGLE_90 or ray_angle == ANGLE_270:
        return 4294967295

    if ray_angle < ANGLE_90 or ray_angle > ANGLE_270:
        vert_x = (p_map[1] * GRID_SIZE) + GRID_SIZE
        vert_y = p_pos[0] + TANTABLE[ray_angle] * (vert_x - p_pos[1])
        next_x = GRID_SIZE
        next_y = STEPTABLE_Y[ray_angle]
    else:
        vert_x = (p_map[1] * GRID_SIZE)
        vert_y = p_pos[0] + TANTABLE[ray_angle] * (vert_x - p_pos[1])
        vert_x -= 1
        next_x = -GRID_SIZE
        next_y = STEPTABLE_Y[ray_angle]

    while True:
        gy = int(vert_y / GRID_SIZE)
        gx = int(vert_x / GRID_SIZE)
        if (gy < 0 or gy > GRID_H) or (gx < 0 or gx > GRID_W):
            return 2147483647
        if MAP[gy][gx] == '1':
            ret = (vert_y - p_pos[0]) * SINTABLE_I[ray_angle]
            return ret
        vert_y += next_y
        vert_x += next_x


def engine_raycast():
    x = ray_ver = ray_hor = 0
    ray_angle = p_angle - ANGLE_30
    while x < WIN_W:
        ray_angle = angle_overflow(ray_angle)
        ray_ver = hit_ver(int(ray_angle))
        ray_hor = hit_hor(int(ray_angle))

#        if ray_angle == 444:
#            print("v :: " + str(ray_ver))
#            print("h :: " + str(ray_hor))

        wall_vert = 0
        if ray_hor <= ray_ver:
            distance = ray_hor
        else: 
            distance = ray_ver
            wall_vert = 1
#        if distance < 0: 
#            print("ray_angle :: " + str(ray_angle) + " | distance :: " + str(distance))
#            if distance == ray_hor: print("RAY_HOR")
#            else: print("RAY_VER")
#            print(ANGLE_360)
#            print(distance)
#            distance = -distance
        distance = distance * FISHTABLE[x]
        wall_len = GRID_SIZE * PLANE_DIST / distance
        wall_bot = HORIZON + (int(wall_len) * 0.5)
        wall_top = HORIZON - (int(wall_len) * 0.5)

#        if wall_len <= 0:
#            print("")
#            print("distance :: " + str(distance))
#            print("ray_angle :: " + str(ray_angle))
#            print("plane dist :: " + str(PLANE_DIST))
#            print("horizon :: " + str(HORIZON))
#            print("wall_len :: " + str(wall_len))
#            print("wall_bot :: " + str(wall_bot))
#            print("wall_top :: " + str(wall_top))

        if wall_top < 0: wall_top = 0
        if wall_bot > WIN_H: wall_bot = WIN_H - 1
        draw_column(x, int(wall_top), int(wall_bot), int(wall_len), wall_vert, int(ray_angle))
        ray_angle += 1
        x += 1

    SDL_RenderPresent(RENDERER);
    SDL_SetRenderDrawColor(RENDERER, 0, 0, 0, 0);
    SDL_RenderClear(RENDERER);
    return 0


#==============================================================================#
#------------------------------------------------------------------------------#
# Draw Tool

def fishtable_create():
    ret = [None] * (ANGLE_60 + 1)
    i = -ANGLE_30
    while i <= ANGLE_30:
        radian = i * math.pi  / (180 * ANGLE_INC) + 0.0001
        ret[i + ANGLE_30] = math.cos(radian)
        i += 1
    return ret

def draw_column(x, y_top, y_bot, length, wall_vert, ray_angle):
#    print("top :: "+ str(y_top) + " bot :: " + str(y_bot))
#    if length >= WIN_H: length = WIN_H
#    color = int(255 - ((length * 100) / WIN_H))
#    if color < 0: color = 0

    if wall_vert == 1 and ray_angle > ANGLE_90 and ray_angle < ANGLE_270:
        SDL_SetRenderDrawColor(RENDERER, 255, 255, 255, 255);
    elif wall_vert == 1: 
        SDL_SetRenderDrawColor(RENDERER, 255, 0, 0, 255);
    elif wall_vert == 0 and ray_angle > 0 and ray_angle < ANGLE_180:
        SDL_SetRenderDrawColor(RENDERER, 0, 255, 0, 255);
    else:
        SDL_SetRenderDrawColor(RENDERER, 255, 0, 255, 255);

    while y_top < y_bot:
        SDL_RenderDrawPoint(RENDERER, x, y_top)
        y_top += 1

#==============================================================================#
#------------------------------------------------------------------------------#
# REGULAR FUNCTION

def degree_to_radian(val):
    return val * math.pi / 180

def radian_to_degree(val):
    return val * 180 / math.pi

def angle_overflow(angle):
    if angle >= ANGLE_0 and angle < ANGLE_360:
        return angle
    if angle < ANGLE_0:
        return ANGLE_360 + angle
    else:
        return angle - ANGLE_360

def get_p_pos(MAP):
    ret = [None] * 2
    for y in range(len(MAP)):
        for x in range(len(MAP[1])):
            if MAP[y][x] == 'z':
                ret[0] = (y + 0.5) * GRID_SIZE
                ret[1] = (x +0.5) * GRID_SIZE
    return ret

def get_p_map(MAP):
    ret = [None] * 2
    for y in range(len(MAP)):
        for x in range(len(MAP[1])):
            if MAP[y][x] == 'z':
                ret[0] = y
                ret[1] = x
    return ret

def update_p_map():
    global p_map
    p_map[0] = int(p_pos[0] / GRID_SIZE)
    p_map[1] = int(p_pos[1] / GRID_SIZE)

def get_map():
    if len(sys.argv) != 2:
        print("Error No MAP given");
        sys.exit(-1)
    input_file = open(sys.argv[1])
    ret = []
    print("Map blueprint: ")
    for line in input_file:
        line = line.strip()
        lst = line.split(" ")
        ret.append(lst)
        print(lst)
    return ret

def run():
    running = True

    global p_angle
    engine_raycast()
    while running:
        events = SDL_Event()
        while SDL_PollEvent(events):
            if events.type == SDL_QUIT or events.type == 256:
                print("-- Time to Quit")
                running = False
                break
            if events.type == SDL_KEYDOWN or events.type == 771:
#                print("-- Key Pressed")
#                print(events.key.keysym.scancode)
                key = events.key.keysym.scancode
                if keystatus[sdl2.SDL_SCANCODE_W]: 
#                    print("You had pressed: 'w'")
                    p_pos[0] += round(SINTABLE[p_angle] * 5); 
                    p_pos[1] += round(COSTABLE[p_angle] * 5); 
                    p_map = update_p_map()
                    engine_raycast()
                if keystatus[sdl2.SDL_SCANCODE_S]: 
#                    print("You had pressed: 's'")
                    p_pos[0] -= round(SINTABLE[p_angle] * 5); 
                    p_pos[1] -= round(COSTABLE[p_angle] * 5); 
                    p_map = update_p_map()
                    engine_raycast()
                if keystatus[sdl2.SDL_SCANCODE_A]: 
#                    print("You had pressed: 'a'")
                    p_angle = angle_overflow(p_angle - 160)
                    engine_raycast()
                if keystatus[sdl2.SDL_SCANCODE_D]: 
#                    print("You had pressed: 'd'")
                    p_angle = angle_overflow(p_angle + 160)
                    engine_raycast()
            break
        SDL_WaitEvent(SDL_Event())
    SDL_DestroyRenderer(RENDERER);
    SDL_DestroyWindow(WINDOW)
    SDL_Quit()
    return 0

#==============================================================================#
#------------------------------------------------------------------------------#
# INIT VAR

#WIN_W= 960
#WIN_H= 600
#WIN_W= 640
#WIN_H= 400
WIN_W= 320
WIN_H= 200
MAP= get_map()
GRID_H= len(MAP) - 1
GRID_W= len(MAP[1]) - 1
GRID_SIZE = 64

FOV = 60

HORIZON = int(WIN_H / 2)
PLANE_DIST = int((WIN_W / 2) / math.tan(degree_to_radian(FOV / 2)))

ANGLE_INC = WIN_W / FOV

ANGLE_0 = 0
ANGLE_60 = WIN_W
ANGLE_30 = int(ANGLE_60 / 2)
ANGLE_90 = int(ANGLE_60 + ANGLE_30)
ANGLE_180 = int(ANGLE_90 * 2)
ANGLE_270 = int(ANGLE_180 + ANGLE_90)
ANGLE_360 = int(ANGLE_180 * 2)

TANTABLE = [None] * (ANGLE_360 + 1)
COSTABLE = [None] * (ANGLE_360 + 1)
SINTABLE = [None] * (ANGLE_360 + 1)
TANTABLE_I = [None] * (ANGLE_360 + 1)
COSTABLE_I = [None] * (ANGLE_360 + 1)
SINTABLE_I = [None] * (ANGLE_360 + 1)
STEPTABLE_Y = [None] * (ANGLE_360 + 1)
STEPTABLE_X = [None] * (ANGLE_360 + 1)
FISHTABLE = fishtable_create()

p_map = get_p_map(MAP)
p_pos = get_p_pos(MAP)
p_angle = 1440
keystatus = sdl2.SDL_GetKeyboardState(None)
#p_angle = 480
p_speed = 16

# iterate on 360 *(win_w/fov)
for i in range(ANGLE_360 + 1):
    radian = (i * math.pi) / ANGLE_180 + 0.0001
    TANTABLE[i] = (math.tan(radian))
    TANTABLE_I[i] = (1.0 / TANTABLE[i])
    COSTABLE[i] = (math.cos(radian))
    COSTABLE_I[i] = (1.0 / COSTABLE[i])
    SINTABLE[i] = (math.sin(radian))
    SINTABLE_I[i] = (1.0 / SINTABLE[i])
    STEPTABLE_X[i] = (GRID_SIZE / TANTABLE[i])
    STEPTABLE_Y[i] = (GRID_SIZE * TANTABLE[i])

    if i >= ANGLE_90 and i < ANGLE_270:         #face LEFT
        if STEPTABLE_X[i] > 0: STEPTABLE_X[i] = -STEPTABLE_X[i]
    else:                                       #face RIGHT
        if STEPTABLE_X[i] < 0: STEPTABLE_X[i] = -STEPTABLE_X[i]

    if i >= ANGLE_0 and i < ANGLE_180:          #face DOWN
        if STEPTABLE_Y[i] < 0: STEPTABLE_Y[i] = -STEPTABLE_Y[i]
    else:                                       #face UP
        if STEPTABLE_Y[i] > 0: STEPTABLE_Y[i] = -STEPTABLE_Y[i]


print("")
print("HORIZON: " + str(HORIZON))
print("PLANE DISTANCE: " + str(PLANE_DIST))
print("Map Size: " + str(GRID_H) + "x" + str(GRID_W))
print("Player Pixel Position:" + str(p_pos))
print("Player MAP Position:" + str(p_map))
print("")


#==============================================================================#
#------------------------------------------------------------------------------#
# INIT SDL

SDL_Init(SDL_INIT_VIDEO)
WINDOW = SDL_CreateWindow(b"Raycast",
    SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
    WIN_W, WIN_H, SDL_WINDOW_SHOWN)

SDL_SetWindowResizable(WINDOW, SDL_FALSE);

RENDERER = SDL_CreateRenderer(WINDOW, -1, SDL_RENDERER_ACCELERATED);

SDL_RenderPresent(RENDERER);
SDL_SetRenderDrawColor(RENDERER, 0, 0, 0, 0);
SDL_RenderClear(RENDERER);

#test
SDL_SetRenderDrawColor(RENDERER, 255, 0, 0, 255);
#draw_column(700, 200, 200)

SDL_RenderPresent(RENDERER);
SDL_SetRenderDrawColor(RENDERER, 0, 0, 0, 0);
SDL_RenderClear(RENDERER);

if __name__ == "__main__":
    sys.exit(run())
